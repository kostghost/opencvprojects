#include <opencv2\core.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\highgui.hpp>

using namespace cv;
int main(int argc, char** argv)
{
	Mat image = cv::imread("..\\example.jpg"); //��������� ����������� � �����

	std::vector<Mat> channels;
	split(image, channels);

	//��������� ��������� ������ ������
	Mat image2;
	threshold(channels[0], image2,
		100, //�����
		255, //����. �������� ����������, ����� �������������� �������� ����� 0 � 255
		CV_THRESH_BINARY); //�������� �����������, (value > 100) ? 255 : 0;
	imshow("Thresholded", image2);
	waitKey(0);

	waitKey(0); //���� ������� �������
}
