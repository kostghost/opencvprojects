#pragma once

#include <opencv2\core.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\highgui.hpp>
using namespace cv;
using namespace std;

#include <stdlib.h>
#include <stdio.h>
#include <iostream>

const int DesireHue = 40;
const int DesireHueRange = 13;
const int DesireMinSaturation = 35;
const int DesireMinValue = 100;

const uchar NotAMarkerValue = 10;
const uchar MarkerValue = 200;

void printCaptureInfo(const VideoCapture &cap){

	double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
	double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

	cout << "Frame size : " << dWidth << " x " << dHeight << endl;
}

//return count of marked pixels
int markDesirePixels(Mat inputFrame, Mat resultFrame, Point2i *gravityCenter) {

	//convert to hsv
	Mat hsvFrame;
	cvtColor(inputFrame, hsvFrame, CV_BGR2HSV);

	*gravityCenter = Point2i(0, 0);
	int markedPointsCount = 0;

	//draw pixels on ResultFrame
	for (int x = 0; x < hsvFrame.cols; x++)
	{
		for (int y = 0; y < hsvFrame.rows; y++)
		{
			if (hsvFrame.at<cv::Vec3b>(y, x)[0] > DesireHue - DesireHueRange &&
				hsvFrame.at<cv::Vec3b>(y, x)[0] < DesireHue + DesireHueRange &&
				hsvFrame.at<cv::Vec3b>(y, x)[1] > DesireMinSaturation &&
				hsvFrame.at<cv::Vec3b>(y, x)[2] > DesireMinValue)
			{
				resultFrame.at<uchar>(y, x) = NotAMarkerValue;
				markedPointsCount++;
				gravityCenter->x += x;
				gravityCenter->y += y;
			}
			else {
				resultFrame.at<uchar>(y, x) = 0;
			}
		}
	}
	
	if (markedPointsCount != 0) {
		gravityCenter->x /= markedPointsCount;
		gravityCenter->y /= markedPointsCount; 
	}
	else {
		return 0;
	}
	
	return markedPointsCount;
}

int mainProcess(VideoCapture &cap) {

	Mat inputFrame;

	//read frame and check
	if ( !cap.read(inputFrame) ) {
		cout << "Cannot read a frame from video stream" << endl;
		return -1;
	}
	
	Mat resultFrame = Mat(inputFrame.size(), CV_8U);

	Point2i gravityCenter;
	int markedPixelsCount = markDesirePixels(inputFrame, resultFrame, &gravityCenter);

	if (markedPixelsCount > 0) 
	{
		erode(resultFrame, resultFrame, NULL, cv::Point(-1, -1), 3);
		dilate(resultFrame, resultFrame, NULL, cv::Point(-1, -1), 5);
	
		Rect boundingBoxRect;
		floodFill(resultFrame, gravityCenter, cv::Scalar(MarkerValue, 0), &boundingBoxRect, cv::Scalar(NotAMarkerValue - 1, 0), cv::Scalar(NotAMarkerValue + 1, 0));

		rectangle(inputFrame, boundingBoxRect, cv::Scalar(255, 0, 0, 0));
	}

	imshow("My Video", inputFrame);
	imshow("ModifiedVideo", resultFrame);

	if (waitKey(30) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
	{
		cout << "esc key is pressed by user" << endl;
		return -1;
	}

	return 0;
}

void captureFromCamera() {

	VideoCapture cap(0); // open the video camera no. 0
	if (!cap.isOpened())  // if not success, exit program
	{
		cout << "Cannot open the video cam" << endl;
		return;
	}

	printCaptureInfo(cap);

	while ( !mainProcess(cap) ) {
	}
}
